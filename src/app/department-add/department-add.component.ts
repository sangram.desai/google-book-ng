import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GooglebooksearchService } from '../services/googlebooksearch.service'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: 'app-department-add',
  templateUrl: './department-add.component.html',
  styleUrls: ['./department-add.component.css']
})

export class DepartmentAddComponent implements OnInit {
  id: string;
  myFormGroup: FormGroup;

  constructor(private route: ActivatedRoute,
    private googlebooksearchService: GooglebooksearchService,
    private router: Router,
    private http: HttpClient,
    public fb: FormBuilder,
    private el: ElementRef) {

  }

  ngOnInit() {
    this.myFormGroup = this.fb.group(
      {
        deptName: [null],
        deptImage: [null]
      })
  }

  onSubmit() {
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#deptImage');
    let fileCount: number = inputEl.files.length;

    var formData: any = new FormData();
    formData.append("departmentName", this.myFormGroup.get('deptName').value);

    if (fileCount > 0) {
      formData.append('departmentImage', inputEl.files.item(0));
    }

    this.http.post('http://localhost:1337/department', formData)
      .subscribe(res => {
        console.log(res);
        setTimeout(() => {
          this.router.navigate(['/department/list']);
        }
          , 5000);
      })
  }


}
