import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GooglebooksearchService } from '../services/googlebooksearch.service'
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: 'app-department-edit',
  templateUrl: './department-edit.component.html',
  styleUrls: ['./department-edit.component.css']
})



export class DepartmentEditComponent implements OnInit {
  id: string;
  dept: any;
  myFormGroup: FormGroup;


  constructor(private route: ActivatedRoute,
    private googlebooksearchService: GooglebooksearchService,
    private router: Router,
    private http: HttpClient,
    public fb: FormBuilder,
    private el: ElementRef) {

    this.id = this.route.snapshot.paramMap.get("id");
    let url = 'http://localhost:1337/department/' + this.id;

    this.googlebooksearchService.callHttpGet(url).subscribe((data: any) => {
      this.dept = data;

      this.myFormGroup = this.fb.group(
        {
          deptName: [this.dept.departmentName],
          deptImage: [null]
        })
    })


  }

  ngOnInit() {
  }




  onSubmit() {
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#deptImage');
    let fileCount: number = inputEl.files.length;

    var formData: any = new FormData();
    formData.append("departmentName", this.myFormGroup.get('deptName').value);

    if (fileCount > 0) {
      formData.append('departmentImage', inputEl.files.item(0));
    }

    this.http.put('http://localhost:1337/department/' + this.id, formData)
      .subscribe(res => {
        setTimeout(() => {
          this.router.navigate(['/department/list']);
        }
          , 5000);
      })
  }



}
