import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})


export class GooglebooksearchService {

  constructor(private httpClient: HttpClient) {

  }

  callHttpGet(url) {
    const result = this.httpClient.get(url);
    return result;
  }

  CallHttpDelete(url) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    return new Promise(resolve => {
      this.httpClient.delete(url, httpOptions)
        .subscribe(res => {
          resolve(res);
        }, err => {
          resolve(err);
        });
    });
  }


}
