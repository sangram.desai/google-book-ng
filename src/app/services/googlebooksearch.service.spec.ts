import { TestBed } from '@angular/core/testing';

import { GooglebooksearchService } from './googlebooksearch.service';

describe('GooglebooksearchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GooglebooksearchService = TestBed.get(GooglebooksearchService);
    expect(service).toBeTruthy();
  });
});
