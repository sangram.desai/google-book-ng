import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';

import {GooglebooksearchService} from './services/googlebooksearch.service';
import { DepartmentComponent } from './department/department.component';
import { DepartmentDeleteComponent } from './department-delete/department-delete.component';
import { DepartmentEditComponent } from './department-edit/department-edit.component'

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { DepartmentAddComponent } from './department-add/department-add.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    DepartmentComponent,
    DepartmentDeleteComponent,
    DepartmentEditComponent,
    DepartmentAddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  providers: [
    GooglebooksearchService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
