import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { DepartmentComponent } from './department/department.component';
import { DepartmentDeleteComponent } from './department-delete/department-delete.component';
import { DepartmentEditComponent } from './department-edit/department-edit.component';
import { DepartmentAddComponent } from './department-add/department-add.component';

const routes: Routes = [
  {
    path: 'book/search',
    component: SearchComponent
  },
  {
    path: 'department/list',
    component: DepartmentComponent
  },
  {
    path: 'department/delete/:id',
    component: DepartmentDeleteComponent
  },
  {
    path: 'department/edit/:id',
    component: DepartmentEditComponent
  },
  {
    path: 'department/add',
    component: DepartmentAddComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
