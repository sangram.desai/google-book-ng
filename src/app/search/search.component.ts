import { Component, OnInit } from '@angular/core';
import { GooglebooksearchService } from '../services/googlebooksearch.service'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})


export class SearchComponent implements OnInit {
  books = [];

  constructor(private googlebooksearchService: GooglebooksearchService) { }

  ngOnInit() {
  }


  onClick(searchText: string) {
    console.log(searchText);
    let url = 'https://www.googleapis.com/books/v1/volumes?q=' + searchText;
    console.log(url);


    this.googlebooksearchService.callHttpGet(url).subscribe((data: any) => {

      console.log(data);
      this.books=[];
      for (let i = 0; i < data.items.length; i++) {

        this.books.push({
          "authors": (data.items[i].volumeInfo.authors) ? data.items[i].volumeInfo.authors.join(' , '):'',
          "title": data.items[i].volumeInfo.title,
          "thumbnail": data.items[i].volumeInfo.imageLinks.thumbnail,
          "infoLink": data.items[i].volumeInfo.infoLink
        });
        
      }

      console.log(this.books);

    })


  }
}
