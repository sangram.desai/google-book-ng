import { Component, OnInit } from '@angular/core';
import { GooglebooksearchService } from '../services/googlebooksearch.service'

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
  depts = [];

  constructor(private googlebooksearchService: GooglebooksearchService) {

  }

  ngOnInit() {
    let url = 'http://localhost:1337/department';
    this.googlebooksearchService.callHttpGet(url).subscribe((data: any) => {

      console.log(data);
      this.depts = [];

      for (let i = 0; i < data.length; i++) {
        this.depts.push({
          "id": data[i].id,
          "departmentName": data[i].departmentName,
          "departmentImage": data[i].departmentImage,
        });
      }
      console.log(this.depts);


    })
  }

}
