import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { GooglebooksearchService } from '../services/googlebooksearch.service'

@Component({
  selector: 'app-department-delete',
  templateUrl: './department-delete.component.html',
  styleUrls: ['./department-delete.component.css']
})

export class DepartmentDeleteComponent implements OnInit {
  id: string;
  constructor(private route: ActivatedRoute, private googlebooksearchService: GooglebooksearchService, private router: Router, ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get("id");

    console.log("http://localhost:1337/department/" + this.id)

    if (this.id) {
      this.googlebooksearchService.CallHttpDelete("http://localhost:1337/department/" + this.id);
    }

    setTimeout(() => {
      this.router.navigate(['/department/list']);
    }
      , 5000);
  }

}
